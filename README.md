## Algorithms: Design and Analysis, Part 1 assignments

Assignments solutins for course "Algorithms: Design and Analysis, Part 1" from
Coursera web. All the assignments are writen in Java.

# Add to Eclipse

Gradle wrapper is used for building. You do not need to have installed Gradle. 
In Eclipse gradle plugin is neede. To add  project to Eclipse workspace just use
Import > Gradle > Gradle project.