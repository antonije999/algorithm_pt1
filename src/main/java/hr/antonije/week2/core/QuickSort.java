package hr.antonije.week2.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.antonije.util.FileReaderUtil;

/**
 * The class containing implementation of quicksort algorithm as explained in course lectures.
 * Class contains static {@code main} method used for starting the second assignment.
 * <p>It is also possible to use just the quicksort algorithm implementation, which is exposed in 
 * {@link #quickSort(List) quickSort} method.
 * 
 * @author Antonije Ivanovic
 */
public class QuickSort {
	
	private static final Logger LOG = LoggerFactory.getLogger(QuickSort.class);
	
	/**
	 * Variable for counting number of comparisons
	 * used in quicksort.
	 */
	public static long comparisonCounter = 0;

	/**
	 * Runs the assignment and prints final result to stdout. 
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		List<Long> dataList = new ArrayList<>();
		try {
			dataList = FileReaderUtil.readNumberFile("/week2/data_week2.txt");
		} catch (IOException e) {
			LOG.error("Error while reading data.", e);
		}
		
		List<Long> dataListCopy = new ArrayList<>(dataList);
		
		quickSort(dataList);
		
		// Used to check if sorting is working correctly
		Collections.sort(dataListCopy);
		
		System.out.println("Sorting is " + dataList.equals(dataListCopy));
		System.out.println(comparisonCounter + "");
		
	}
	
	
	/**
	 * Sorts {@code list} using quicksort algorithm in <i>nlog(n)</i> time.
	 * Does not create new list but changes the given list, which is sorted
	 * after this method.
	 * 
	 * @param list containing {@link Comparable} objects
	 */
	public static <T extends Comparable<T>> void quickSort(List<T> list) {
		int listSize = list.size();

		//if list has 1 or 0 elements no need to sort
		if (listSize > 1) {
			int pivotIndex = selectPivot(list);
			T pivot = list.get(pivotIndex);
			T firstElement = list.get(0);
			list.set(0, pivot);
			list.set(pivotIndex, firstElement);
			int i = partitionByPivot(list);
			
			// recurse on list partitions
			quickSort(list.subList(0, i - 1));
			quickSort(list.subList(i, listSize));
		}
	}
	
	/**
	 * Helper method used for selecting pivot for sorting the given list. Pivot is later
	 * used in quicksort algorithm.
	 * 
	 * @param unsortedList for which the pivot is to be selected
	 * @return index of the selected pivot in the {@code unsortedList}
	 */
	private static <T extends Comparable<T>> int selectPivot(List<T> unsortedList) {
		// Pivot index for first assignment
		return 0;
		
		// Pivot index for second assignment
//		return unsortedList.size() - 1;
		
		// Pivot index for third assignment. It is choosen using "median of three"
		// pivot rule.
//		T firstElem = unsortedList.get(0);
//		T lastElem = unsortedList.get(unsortedList.size() - 1);
//		T middleElem = unsortedList.get((int) Math.ceil((double) unsortedList.size() / 2.0) - 1);
//		int medianIndex;
//		
//		if (firstElem.compareTo(lastElem) < 1) {
//			if (firstElem.compareTo(middleElem) < 1) {
//				if (middleElem.compareTo(lastElem) < 1) {
//					medianIndex = (int) Math.ceil((double) unsortedList.size() / 2.0) - 1;
//				} else {
//					medianIndex = unsortedList.size() - 1;
//				}
//			} else {
//				medianIndex = 0; 
//			}
//		} else {
//			if (lastElem.compareTo(middleElem) < 1) {
//				if (middleElem.compareTo(firstElem) < 1) {
//					medianIndex =  (int) Math.ceil((double) unsortedList.size() / 2.0) - 1;
//				} else {
//					medianIndex = 0;
//				}
//			} else {
//				medianIndex = unsortedList.size() - 1;
//			}
//		}
//		
//		return medianIndex;
	}
	
	/**
	 * Partitions {@code list} by pivot. Method assumes that the pivot is the
	 * first element in the given list. It is the caller responsibility to set
	 * pivot to the first element in the list.
	 * 
	 * @param list to be partition by pivot
	 * @return pivot index in the partitioned list
	 */
	private static <T extends Comparable<T>> int partitionByPivot(List<T> list) {
		T pivot = list.get(0);
		int listSize = list.size();
		int i = 1;
		
		// count the number of comparisons for assignment
		comparisonCounter = comparisonCounter + listSize - 1;
		
		for (int j = i; j < listSize; j++) {
			T currentElement = list.get(j);
			
			if (currentElement.compareTo(pivot) < 0) {
				T biggerElement = list.get(i);
				list.set(i, currentElement);
				list.set(j, biggerElement);
				i++;
			}
		}
		
		T swapElement = list.get(i - 1);
		list.set(0, swapElement);
		list.set(i - 1, pivot);
		
		return i;
	}

}
