package hr.antonije.week1.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.antonije.util.FileReaderUtil;

public class MergeSort {
	
	public static final Logger LOG = LoggerFactory.getLogger(MergeSort.class);

	private static long inverseCounter = 0;

	public static void main(String[] args) {
		List<Long> numbersList = new ArrayList<>();

		try {
			numbersList = FileReaderUtil.readNumberFile("/week1/data.txt");
		} catch (IOException e) {
			LOG.error("Error reading a line from file", e);
		}

		List<Long> trueSort = new ArrayList<>(numbersList);
		Collections.sort(trueSort);

		List<Long> sortedList = sort(numbersList);


		if (trueSort.equals(sortedList)) {
			System.out.println("Ispravno sortirano");
		} else {
			System.out.println("Nije ispravno sortirano");
		}

	}

	public static List<Long> sort(List<Long> unsortedList) {
		Long[] sortedArray = mergeSort(unsortedList.toArray(new Long[] {}));
		return Arrays.asList(sortedArray);
	}

	private static Long[] mergeSort(Long[] unsortedList) {
		int unsortedListSize = unsortedList.length;
		if (unsortedListSize == 1) {
			return unsortedList;
		} else {
			int subarrayLength = unsortedListSize / 2;
			Long[] leftSubarray = new Long[subarrayLength];
			System.arraycopy(unsortedList, 0, leftSubarray, 0, subarrayLength);

			Long[] rightSubarray = new Long[unsortedListSize - subarrayLength];
			System.arraycopy(unsortedList, subarrayLength, rightSubarray, 0, unsortedListSize - subarrayLength);

			Long[] sortedLeftList = mergeSort(leftSubarray);
			Long[] sortedRightList = mergeSort(rightSubarray);

			Long[] sortedList = merge(sortedLeftList, sortedRightList);
			return sortedList;
		}

	}

	private static Long[] merge(Long[] leftList, Long[] rightList) {

		int i = 0; // left list current index
		int j = 0; // right list current index
		int leftListSize = leftList.length;
		int rightListSize = rightList.length;
		int forSize = leftListSize + rightListSize;

		Long[] sortedList = new Long[forSize];

		for (int k = 0; k < forSize; k++) {
			if (leftListSize == i) {
				sortedList[k] = rightList[j];
				j++;
			} else if (rightListSize == j) {
				sortedList[k] = leftList[i];
				i++;
			} else if (leftList[i] < rightList[j]) {
				sortedList[k] = leftList[i];
				i++;
			} else {
				sortedList[k] = rightList[j];
				j++;
				setInverseCounter(getInverseCounter() + (leftListSize - i));
			}
		}

		return sortedList;
	}

	/**
	 * @return the inverseCounter
	 */
	public static long getInverseCounter() {
		return inverseCounter;
	}

	/**
	 * @param inverseCounter the inverseCounter to set
	 */
	public static void setInverseCounter(long inverseCounter) {
		MergeSort.inverseCounter = inverseCounter;
	}

}