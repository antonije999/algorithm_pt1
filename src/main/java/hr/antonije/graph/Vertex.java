package hr.antonije.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.OptionalInt;

public class Vertex {
	
	private static final int NO_PATH_DISTANCE = 1000000;
	
	private int id;
	private List<Edge> neigbourghEdges = new ArrayList<>();
	
	private boolean explored;
	private Vertex leader;
	private int finishingTime;
	private int minPathDistance = NO_PATH_DISTANCE;

	public Vertex(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public boolean addEdge(Edge edge) {
		return neigbourghEdges.add(edge);
	}
	
	public Edge removeEdge(int index) {
		return neigbourghEdges.remove(index);
	}
	
	public int getEdgeSize() {
		return neigbourghEdges.size();
	}
	
	public Edge getEdge(int index) {
		return neigbourghEdges.get(index);
	}

	public boolean isExplored() {
		return explored;
	}

	public void setExplored(boolean explored) {
		this.explored = explored;
	}
	
	public Vertex getLeader() {
		return leader;
	}

	public void setLeader(Vertex leader) {
		this.leader = leader;
	}

	public int getFinishingTime() {
		return finishingTime;
	}

	public void setFinishingTime(int finishingTime) {
		this.finishingTime = finishingTime;
	}
	
	public int getMinPath() {
		return minPathDistance;
	}
	
	public void setMinPathDistance(int minPathDistance) {
		this.minPathDistance = minPathDistance;
	}

	public boolean calculateMinDistance(Vertex fromVertex) {
		OptionalInt min = neigbourghEdges.stream().filter(edge -> edge.getTail().equals(fromVertex))
								.mapToInt(edge -> edge.getDistance() + edge.getTail().getMinPath())
								.findFirst();
		
		int alternatePathDistance = min.orElse(NO_PATH_DISTANCE);
		if (alternatePathDistance < minPathDistance) {
			minPathDistance = alternatePathDistance;
			return true;
		}
		
		return false;
	}

	public void joinWith(Vertex vertex) {
		Iterator<Edge> it = neigbourghEdges.iterator();
		while (it.hasNext()) {
			Edge edge = (Edge) it.next();
			
			if (edge.getTail().equals(this)) {
				edge.setTail(vertex);
			} else {
				edge.setHead(vertex);
			}
			
			if (!edge.isCircular())
				vertex.addEdge(edge);
			
		}
		
		vertex.cleanCircularEdges();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vertex other = (Vertex) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "" + id + " edges: " + neigbourghEdges;
	}
	
	private void cleanCircularEdges() {
		for (Iterator<Edge> iterator = neigbourghEdges.iterator(); iterator.hasNext();) {
			Edge edge = (Edge) iterator.next();
			
			if (edge.isCircular()) {
				iterator.remove();
			}
			
		}
	}
	
}
