package hr.antonije.graph;

public class Edge {
	
	private Vertex tailVertex;
	private Vertex headVertex;
	private int distance;
	
	public Edge(Vertex vertOne, Vertex vertTwo) {
		this.tailVertex = vertOne;
		this.headVertex = vertTwo;
	}
	
	public void contract(Vertex erase) {
		if (erase.equals(tailVertex)) {
			tailVertex.joinWith(headVertex);
		} else {
			headVertex.joinWith(tailVertex);
		}
	}
	
	public boolean isCircular() {
		return tailVertex.equals(headVertex);
	}

	public Vertex getTail() {
		return tailVertex;
	}

	public Vertex getHead() {
		return headVertex;
	}
	
	public void setTail(Vertex vertOne) {
		this.tailVertex = vertOne;
	}

	public void setHead(Vertex vertTwo) {
		this.headVertex = vertTwo;
	}
	
	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	//TODO fix so that the vertex order does not matter in equality
	// (i.e. vertexOne and vertexTwo can change places)
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime  + ((tailVertex == null) ? 0 : tailVertex.hashCode()) + ((headVertex == null) ? 0 : headVertex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		if (tailVertex == null) {
			if (other.tailVertex != null)
				return false;
		} else if (!tailVertex.equals(other.tailVertex))
			return false;
		if (headVertex == null) {
			if (other.headVertex != null)
				return false;
		} else if (!headVertex.equals(other.headVertex))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return tailVertex.getId() + " -> " + headVertex.getId();
	}
	
	

}
