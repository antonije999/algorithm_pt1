package hr.antonije.week3.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.antonije.graph.Edge;
import hr.antonije.graph.Vertex;
import hr.antonije.util.FileReaderUtil;

public class RandomizedContraction {
	
	private static final Logger LOG = LoggerFactory.getLogger(RandomizedContraction.class);
	
	private static Random rand = new Random();
	private static int THREAD_POOL_SIZE = Math.max(4, Runtime.getRuntime().availableProcessors());
	private static ExecutorService threadPool;
	private static String path;
	
	static {
		threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
	}

	
	public static void main(String[] args) {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/kargerMinCut.txt");
			
			int minimumCut = countGraphMinimumCut(vertexList, "/week3/kargerMinCut.txt");
			System.out.println("Number of minimum cut is " + minimumCut + ".");
		} catch (IOException e) {
			LOG.error("Error reading data file.", e);
		}

	}
	
	public static int countGraphMinimumCut(List<Vertex> vertexList, String path) throws IOException {
		RandomizedContraction.path = path;
		int minimumCut = Integer.MAX_VALUE;
		int n = vertexList.size();
		int numberOfRepetitions = (int) (n * Math.log(n));
		
		List<Future<Integer>> resultsList = new ArrayList<>();
		
		for (int i = 0; i < numberOfRepetitions; i++) {
			//TODO fix so the graph reading on each iteration is unnecessary
			Future<Integer> resultPromis = threadPool.submit(new GraphHandler());
			resultsList.add(resultPromis);
		}
		
		for (Future<Integer> result : resultsList) {
			try {
				minimumCut = Math.min(minimumCut, result.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		return minimumCut;
	}
	
	private static int randomizedGraphContraction(List<Vertex> vertexList) {
		int vertexNum = vertexList.size();
		for (int i = 0; i < vertexNum - 3; i++) {
			int randomVertexIndex = getRandomListIndex(vertexList.size());
			Vertex vertex = vertexList.get(randomVertexIndex);
			
			int randomEdgeIndex = getRandomListIndex(vertex.getEdgeSize());
			Edge edge = vertex.getEdge(randomEdgeIndex);
			edge.contract(vertex);
			vertexList.remove(randomVertexIndex);
		}
		
		int minCut = vertexList.get(0).getEdgeSize();
		
		return minCut;
	}
	
	private static int getRandomListIndex(int listSize) {
		int size = listSize - 1;
		int randomIndex = (int) Math.round(rand.nextDouble() * size);
		
		return randomIndex;
	}
	
	private static class GraphHandler implements Callable<Integer> {

		@Override
		public Integer call() throws Exception {
			List<Vertex> vertexList = FileReaderUtil.readGraph(path);
			
			int computedMinimumCut = randomizedGraphContraction(vertexList);
			return computedMinimumCut;
		}
		
	}

}
