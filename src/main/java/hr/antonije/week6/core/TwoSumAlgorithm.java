package hr.antonije.week6.core;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.antonije.util.FileReaderUtil;

public class TwoSumAlgorithm {
	
	private static final Logger LOG = LoggerFactory.getLogger(TwoSumAlgorithm.class);

	public static void main(String[] args) {
		try {
			List<Long> numbersList = FileReaderUtil.readNumberFile("/week6/algo1_programming_prob_2sum.txt");
			
			int distinctValues = computeDistincValuesInRange(numbersList, -10000, 10000);
			
			System.out.println("Number of distinc values in range was: " + distinctValues + ".");
		} catch (IOException e) {
			LOG.error("Error reading number file.", e);
		}
	}
	
	// TODO slow. needs performance optimization
	private static int computeDistincValuesInRange(List<Long> listOfNumbers, int rangeFrom, int rangeTo) {
		Map<Long, Long> numberHashTable = listOfNumbers.stream().distinct().collect(Collectors.toMap(num -> num, num -> num));
		long tCounter = 0;
		for (long t = rangeFrom; t <= rangeTo; t++) {
			for (Long x : listOfNumbers) {
				Long y = numberHashTable.get(t - x);
				if (y != null) {
					tCounter++;
					break;
				}
			}
		}

		return (int) tCounter;
	}

}
