package hr.antonije.week6.core;

import java.io.IOException;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.antonije.util.FileReaderUtil;

public class MedianMaintenanceAlgorithm {
	
	private static final Logger LOG = LoggerFactory.getLogger(MedianMaintenanceAlgorithm.class);

	public static void main(String[] args) {
		try {
			List<Integer> intList = FileReaderUtil.readNumberFile("/week6/Median.txt").stream().map(num -> num.intValue()).collect(Collectors.toList());
			
			int medianSum = calculateMedianSum(intList);
			int lastFourDigits = medianSum % 10000;
			
			System.out.println("MedianSum mod 10000 (last 4 digits of Median sum): " + lastFourDigits + ".");
		} catch (IOException e) {
			LOG.error("Error reading file with numbers.", e);
		}
	}
	
	private static int calculateMedianSum(List<Integer> numberList) {
		Queue<Integer> highHalf = new PriorityQueue<>();
		Queue<Integer> lowHalf = new PriorityQueue<>((num1, num2) -> -num1.compareTo(num2));
		
		int medianSum = 0;
		for (int i = 0; i < numberList.size(); i++) {
			Integer num = numberList.get(i);
			
			if (i == 0) {
				lowHalf.add(num);
			} else {
				if (lowHalf.peek().compareTo(num) > 0) {
					lowHalf.add(num);
				} else {
					highHalf.add(num);
				}
			}
			
			
			int sizeDiff = lowHalf.size() - highHalf.size();
			
			if (sizeDiff > 1) {
				highHalf.add(lowHalf.remove());
			} else if (sizeDiff < -1) {
				lowHalf.add(highHalf.remove());
			}
			
			
			// check if current statistics is even or 
			// odd by checking if low bit is set to number
			int statistics = 0;
			if ((i + 1 & 1) == 0 ) {
				statistics = (i + 1) / 2;
			} else {
				statistics = (i + 2) / 2;
			}
			
			if (lowHalf.size() == statistics) {
				medianSum += lowHalf.peek();
			} else {
				medianSum += highHalf.peek();
			}
			
			
		}
		
		return medianSum;
	}

}
