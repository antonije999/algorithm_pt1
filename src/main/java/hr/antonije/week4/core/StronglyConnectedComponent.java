package hr.antonije.week4.core;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hr.antonije.graph.Edge;
import hr.antonije.graph.Vertex;
import hr.antonije.util.FileReaderUtil;

public class StronglyConnectedComponent {
	
	private static final Logger LOG = LoggerFactory.getLogger(StronglyConnectedComponent.class);

	public static void main(String[] args) {
		try {
			List<Vertex> graph = FileReaderUtil.readGraphFromEdgeFile("/week4/SCC.txt");
			
			List<Long> result = computeGraphSCC(graph);

			System.out.println("Graph 5 biggest SCC are: " + result);
		} catch (IOException e) {
			LOG.error("Error while reading data file.", e);
		}

	}
	
	public static List<Long> computeGraphSCC(List<Vertex> graph) {
		depthFirstSearchLoop(graph, true);
		
		List<Vertex> sortedGraph = graph.stream().sorted((v1, v2) -> (v1.getFinishingTime() > v2.getFinishingTime()) ? 1 : -1)
					  .collect(Collectors.toList());
		
		sortedGraph.stream().forEach(vertex -> vertex.setExplored(false));
		depthFirstSearchLoop(sortedGraph, false);
		
		List<Long> result = sortedGraph.stream()
				.collect(Collectors.groupingBy(vertex -> vertex.getLeader(), Collectors.counting()))
				.entrySet()
				.stream()
				.map(entry -> entry.getValue())
				.sorted((l1, l2) -> -l1.compareTo(l2))
				.limit(5)
				.collect(Collectors.toList());
		
		return result;
	}
	
	private static void depthFirstSearchLoop(List<Vertex> graph, boolean searechReversed) {
		 int finishingTime = 0;
		 Deque<Vertex> recursionStack = new ArrayDeque<>();
		 
		 for (int i = graph.size(); i > 0; i--) {
			Vertex currentVertex = graph.get(i - 1);
			if (!currentVertex.isExplored()) {
				recursionStack.push(currentVertex);
				Vertex leader = currentVertex;
				while (!recursionStack.isEmpty()) {
					
					currentVertex = recursionStack.peek();
					currentVertex.setExplored(true);
					currentVertex.setLeader(leader);
					int exploringIndicator = 0;
					for (int j = 0; j < currentVertex.getEdgeSize(); j++) {
						Edge edge = currentVertex.getEdge(j);
						Vertex head = searechReversed ? edge.getTail() : edge.getHead();
						if (!head.isExplored() && !head.equals(currentVertex)) {
							recursionStack.push(head);
							exploringIndicator++;
							break;
						}
					}
					
					if (exploringIndicator == 0) {
						finishingTime++;
						recursionStack.pop().setFinishingTime(finishingTime);
					}
				}
			}
		}
	}
	
}
