package hr.antonije.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import hr.antonije.graph.Edge;
import hr.antonije.graph.Vertex;

public class FileReaderUtil {
	
	// static class
	private FileReaderUtil() {}
	
	public static List<Vertex> readGraph(String filePath) throws IOException {
		return readFileByLine(filePath, FileReaderUtil::readVertexLine);
	}
	
	public static List<Vertex> readGraphFromEdgeFile(String filePath) throws IOException {
		return readFileByLine(filePath, FileReaderUtil::readEdgeLine);
	}
	
	public static List<Vertex> readGraphWithWeight(String filePath) throws IOException {
		return readFileByLine(filePath, FileReaderUtil::readWeightedVertexLine);
	}
	
	public static List<Long> readNumberFile(String filePath) throws IOException{
		return readFileByLine(filePath, FileReaderUtil::readNumber);
	}
	
	public static <T> List<T> readFileByLine(String filePath, BiFunction<String, List<T>, List<T>> readLineMethod) throws IOException {
		String path = Class.class.getResource(filePath).getFile();
		File file = new File(path);
		
		BufferedReader reader = null;
		List<T> objectAccumulator = new ArrayList<>();
		
		// reading of file is in try but no exception is caught.
		// used to properly close reader object.
		try {
			reader = new BufferedReader(new FileReader(file));
			
			String dataLine = null;
			while ((dataLine = reader.readLine()) != null) {
				objectAccumulator = readLineMethod.apply(dataLine, objectAccumulator);
			}
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		
		return objectAccumulator;
	}

	private static List<Vertex> readVertexLine(String dataLine, List<Vertex> vertexList) {
		Vertex anchorVertex = null;
		
		String[] vertexArray = dataLine.split("\\s");
		for (int i = 0; i < vertexArray.length; i++) {
			int vertexId = Integer.parseInt(vertexArray[i]);
			
			if (i == 0) {
				anchorVertex = getVertex(vertexId, vertexList);
			} else {
				int anchorId = anchorVertex.getId();
				if (vertexId > anchorId) {
					Vertex vertex = getVertex(vertexId, vertexList);
					Edge edge = new Edge(anchorVertex, vertex);
					anchorVertex.addEdge(edge);
					vertex.addEdge(edge);
				}
			}
		}
		
		return vertexList;
	}
	
	private static List<Vertex> readEdgeLine(String line, List<Vertex> availableVertices) {
		String[] edgeVertices = line.split("\\s");
		int tailId = Integer.parseInt(edgeVertices[0]);
		int headId = Integer.parseInt(edgeVertices[1]);
		
		Vertex tail = getVertex(tailId, availableVertices);
		Vertex head = getVertex(headId, availableVertices);
		
		Edge edge = new Edge(tail, head);
		tail.addEdge(edge);
		head.addEdge(edge);
		
		return availableVertices;
	}
	
	private static List<Vertex> readWeightedVertexLine(String dataLine, List<Vertex> vertexList) {
		String[] nodes = dataLine.split("\\s");
		int anchorVertexId = Integer.parseInt(nodes[0]);
		Vertex anchorVertex = getVertex(anchorVertexId, vertexList);
		
		for (int i = 1; i < nodes.length; i++) {
			String neighbourVertexData = nodes[i];
			String[] vertexData = neighbourVertexData.split(",");
			int vertexId = Integer.parseInt(vertexData[0]);
			int distance = Integer.parseInt(vertexData[1]);
			
			Vertex vertex = getVertex(vertexId, vertexList);
			Edge edge = new Edge(anchorVertex, vertex);
			edge.setDistance(distance);
			vertex.addEdge(edge);
			anchorVertex.addEdge(edge);
		}

		return vertexList;
	}
	
	private static List<Long> readNumber(String dataLine, List<Long> integerList) {
		Long number = Long.valueOf(dataLine);
		integerList.add(number);

		return integerList;
	}
	
	private static Vertex getVertex(int vertexId, List<Vertex> availableVertices) {
		int maxVertex = availableVertices.size();
		Vertex returnVertex = null;
		
		if (vertexId > maxVertex) {
			for (int i = maxVertex + 1; i <= vertexId; i++) {
				returnVertex = new Vertex(i);
				availableVertices.add(returnVertex);
			}
			
		} else {
			returnVertex = availableVertices.get(vertexId - 1);
		}
		
		return returnVertex;
	}

}
