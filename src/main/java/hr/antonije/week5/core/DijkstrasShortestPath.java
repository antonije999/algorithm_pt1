package hr.antonije.week5.core;

import hr.antonije.graph.Edge;
import hr.antonije.graph.Vertex;
import hr.antonije.util.FileReaderUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DijkstrasShortestPath {
	
	private static final Logger LOG = LoggerFactory.getLogger(DijkstrasShortestPath.class);

	public static void main(String[] args) {
		List<Vertex> vertexList;
		try {
			vertexList = FileReaderUtil.readGraphWithWeight("/week5/dijkstraData.txt");
			
			shortestPath(vertexList);
			List<Integer> indexList = Arrays.asList(7,37,59,82,99,115,133,165,188,197);
			
			vertexList.stream().filter(v -> indexList.contains(v.getId()))
							   .forEach(v -> System.out.print(v.getMinPath() + ","));
		} catch (IOException e) {
			LOG.error("Error reading txt file.", e);
		}
	}
	
	public static void shortestPath(List<Vertex> vertexList) {
		GraphHeap heap = new GraphHeap(vertexList);
		vertexList.get(0).setMinPathDistance(0);
		
		while (heap.hasElements()) {
			heap.getMinElement();
		}
	}
	
	private static class GraphHeap {
		
		List<Vertex> heapData;

		public GraphHeap(List<Vertex> data) {
			heapData = new ArrayList<>(data);
		}
		
		public Vertex getMinElement() {
			Vertex minElement = heapData.get(0);
			minElementProcessing(minElement);
			
			bubbleDownMain();
			return minElement;
		}
		
		private void minElementProcessing(Vertex minElement) {
			for (int i = 0; i < minElement.getEdgeSize(); i++) {
				Edge edge = minElement.getEdge(i);
				if (edge.getTail().equals(minElement)) {
					Vertex headEdge = edge.getHead();
					if (heapData.contains(headEdge)) {
						boolean changed = headEdge.calculateMinDistance(minElement);
						if (changed) {
							bubbleUp(headEdge);
						}
					}
				}
			}
		}
		
		private void bubbleUp(Vertex vertex) {
			int index = heapData.indexOf(vertex);
			int parentIndex = (int) Math.floor((index)  / 2);
			Vertex parent = heapData.get(parentIndex);
			
			if (parent.getMinPath() > vertex.getMinPath()) {
				heapData.remove(index);
				heapData.add(index, parent);
				heapData.remove(parentIndex);
				heapData.add(parentIndex, vertex);
				
				bubbleUp(vertex);
			}
		}
		
		private Vertex bubbleDownMain() {
			Vertex lastVertex = heapData.remove(heapData.size() - 1);
			Vertex minElement = heapData.remove(0);
			heapData.add(0, lastVertex);
			
			if (heapData.size() > 2) {
				bubbleDown(1);
			} else {

				if (heapData.size() == 2) {
					Vertex one = heapData.get(0);
					Vertex two = heapData.get(1);
					if (two.getMinPath() < one.getMinPath()) {
						heapData.remove(0);
						heapData.add(one);
					}
				} else if (heapData.size() == 1) {
					return heapData.remove(0);
				}
			}
			
			return minElement;
		}
		
		private void bubbleDown(int index) {
			int indexOne = 2 * index - 1;
			int indexTwo = indexOne + 1;
			
			if (heapData.size() <= indexOne) {
				return;
			}
			Vertex chiledOne = heapData.get(indexOne);
			
			Vertex chiledTwo = null;
			if (heapData.size() <= indexTwo) {
				chiledTwo = new Vertex(1000);
			} else {
				chiledTwo = heapData.get(indexTwo);
			}
			
			int replaceIndex = -1;
			Vertex replaceVertex = null;
			if (chiledOne.getMinPath() < chiledTwo.getMinPath()) {
				replaceIndex = indexOne;
				replaceVertex = chiledOne;
			} else {
				replaceIndex = indexTwo;
				replaceVertex = chiledTwo;
			}
			
			if (replaceVertex.getMinPath() < heapData.get(index - 1).getMinPath()) {
				Vertex biggerVertex = heapData.remove(index - 1);
				heapData.add(index - 1, replaceVertex);
				heapData.remove(replaceIndex);
				heapData.add(replaceIndex, biggerVertex);
				
				bubbleDown(replaceIndex + 1);
			}
		}
		
		public boolean hasElements() {
			return heapData.size() > 0;
		}
		
	}

}
