package hr.antonije.week1.test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import hr.antonije.week1.core.MergeSort;

public class MergeSortTest {
	
	private List<Long> testData;
	

	@Test
	public void testMergeSortCorrectnes() {
		MergeSort.setInverseCounter(0);
		List<Long> list = Arrays.asList(2L, 5L, 8L, 0L, 3L, 1L, 7L, 4L, 12L, 20L);
		List<Long> listCopy = new ArrayList<>(list);
		
		list = MergeSort.sort(list);
		Collections.sort(listCopy);
		
		assertThat(list, equalTo(listCopy));
		
	}
	
	@Test
	public void testMergeSortCounterOne() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(1L,3L,5L,2L,4L,6L);
		
		MergeSort.sort(testData);
		
		assertEquals(3, MergeSort.getInverseCounter());
	}
	
	@Test
	public void testMergeSortCounterTwo() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(1L,5L,3L,2L,4L);

		MergeSort.sort(testData);
		
		assertEquals(4, MergeSort.getInverseCounter());
		
	}
	
	@Test
	public void testMergeSortCounterThree() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(5L,4L,3L,2L,1L);

		MergeSort.sort(testData);
		
		assertEquals(10, MergeSort.getInverseCounter());
	}
	
	@Test
	public void testMergeSortCounterFour() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(1L,6L,3L,2L,4L,5L);

		MergeSort.sort(testData);
		
		assertEquals(5, MergeSort.getInverseCounter());
		
	}
	
	@Test
	public void testMergeSortCounterFive() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(9L, 12L, 3L, 1L, 6L, 8L, 2L, 5L, 14L, 13L, 11L, 7L, 10L, 4L, 0L);

		MergeSort.sort(testData);
		
		assertEquals(56, MergeSort.getInverseCounter());
	}
	
	@Test
	public void testMergeSortCounterSix() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(37L, 7L, 2L, 14L, 35L, 47L, 10L, 24L, 44L, 17L, 34L, 11L, 16L,
				48L, 1L, 39L, 6L, 33L, 43L, 26L, 40L, 4L, 28L, 5L, 38L, 41L, 42L, 12L, 13L, 21L, 29L,
				18L, 3L, 19L, 0L, 32L, 46L, 27L, 31L, 25L, 15L, 36L, 20L, 8L, 9L, 49L, 22L, 23L, 30L, 45L);

		MergeSort.sort(testData);
		
		assertEquals(590, MergeSort.getInverseCounter());
	}
	
	@Test
	public void testMergeSortCounterSeven() {
		MergeSort.setInverseCounter(0);
		testData = Arrays.asList(4L, 80L, 70L, 23L, 9L, 60L, 68L, 27L, 66L, 78L, 12L, 40L, 52L,
				53L, 44L, 8L, 49L, 28L, 18L, 46L, 21L, 39L, 51L, 7L, 87L, 99L, 69L, 62L, 84L, 6L, 79L, 67L,
				14L, 98L, 83L, 0L, 96L, 5L, 82L, 10L, 26L, 48L, 3L, 2L, 15L, 92L, 11L, 55L, 63L, 97L, 43L,
				45L, 81L, 42L, 95L, 20L, 25L, 74L, 24L, 72L, 91L, 35L, 86L, 19L, 75L, 58L, 71L, 47L, 76L,
				59L, 64L, 93L, 17L, 50L, 56L, 94L, 90L, 89L, 32L, 37L, 34L, 65L, 1L, 73L, 41L, 36L, 57L, 77L,
				30L, 22L, 13L, 29L, 38L, 16L, 88L, 61L, 31L, 85L, 33L, 54L);

		MergeSort.sort(testData);
		
		assertEquals(2372, MergeSort.getInverseCounter());
	}

}
