package hr.antonije.week2.test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import hr.antonije.week2.core.QuickSort;

public class QuickSortTest {
	
	private static List<Integer> dataListOne;
	private static List<Integer> dataListTwo;
	private static List<Integer> dataListThree;
	
	@BeforeClass
	public static void init() {
		InputStream in1 = QuickSortTest.class.getClassLoader().getResourceAsStream("week2/10.txt");
		InputStream in2 = QuickSortTest.class.getClassLoader().getResourceAsStream("week2/100.txt");
		InputStream in3 = QuickSortTest.class.getClassLoader().getResourceAsStream("week2/1000.txt");
		
		BufferedReader data1 = new BufferedReader(new InputStreamReader(in1));
		BufferedReader data2 = new BufferedReader(new InputStreamReader(in2));
		BufferedReader data3 = new BufferedReader(new InputStreamReader(in3));
		
		try {
			dataListOne = readData(data1);
			dataListTwo = readData(data2);
			dataListThree = readData(data3);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testQuickSortCorrectnes() {
		List<Integer> unsortedList = Arrays.asList(2, 5, 8, 10, 9, 4, 7, 1);
		List<Integer> unsortedListCopy = new ArrayList<Integer>(unsortedList);
		QuickSort.comparisonCounter = 0;
		
		Collections.sort(unsortedListCopy);
		QuickSort.quickSort(unsortedList);
		
		assertThat("quickSort() not sorting correctly", unsortedList, equalTo(unsortedListCopy));
	}
	
	@Test
	public void testQuicSortOne() {
		QuickSort.comparisonCounter = 0;

		QuickSort.quickSort(dataListOne);
		
		assertEquals("Number of comparisons in quick sort not good.", 25, QuickSort.comparisonCounter);
	}
	
	@Test
	public void testQuicSortTwo() {
		QuickSort.comparisonCounter = 0;

		QuickSort.quickSort(dataListTwo);
		
		assertEquals("Number of comparisons in quick sort not good.", 615, QuickSort.comparisonCounter);
	}
	
	@Test
	public void testQuicSortThree() {
		QuickSort.comparisonCounter = 0;

		QuickSort.quickSort(dataListThree);
		
		assertEquals("Number of comparisons in quick sort not good.", 10297, QuickSort.comparisonCounter);
	}


	/*
	 *Used to read data from file to list. Every line in file
	 *is read as integer and added to list.
	 */
	private static List<Integer> readData(BufferedReader data) throws NumberFormatException, IOException {
		String line = null;
		List<Integer> list = new ArrayList<Integer>();
		while ((line = data.readLine()) != null) {
			list.add(Integer.valueOf(line));
		}
		
		return list;
	}

}
