package hr.antonije.week4.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import hr.antonije.graph.Vertex;
import hr.antonije.util.FileReaderUtil;
import hr.antonije.week4.core.StronglyConnectedComponent;

public class StronglyConnectedComponentTest {
	
	@Test
	public void testDataOne() throws Exception {
		List<Vertex> graph = FileReaderUtil.readGraphFromEdgeFile("/week4/test1.txt");
		
		List<Long> graphSCCs = StronglyConnectedComponent.computeGraphSCC(graph);
		
		assertEquals(Arrays.asList(3L, 3L, 3L), graphSCCs);
	}
	
	@Test
	public void testDataTwo() throws Exception {
		List<Vertex> graph = FileReaderUtil.readGraphFromEdgeFile("/week4/test2.txt");
		
		List<Long> graphSCCs = StronglyConnectedComponent.computeGraphSCC(graph);
		
		assertEquals(Arrays.asList(3L, 3L, 2L), graphSCCs);
	}
	
	@Test
	public void testDataThree() throws Exception {
		List<Vertex> graph = FileReaderUtil.readGraphFromEdgeFile("/week4/test3.txt");
		
		List<Long> graphSCCs = StronglyConnectedComponent.computeGraphSCC(graph);
		
		assertEquals(Arrays.asList(3L, 3L, 1L, 1L), graphSCCs);
	}
	
	@Test
	public void testDataFour() throws Exception {
		List<Vertex> graph = FileReaderUtil.readGraphFromEdgeFile("/week4/test4.txt");
		
		List<Long> graphSCCs = StronglyConnectedComponent.computeGraphSCC(graph);
		
		assertEquals(Arrays.asList(7L, 1L), graphSCCs);
	}
	
	@Test
	public void testDataFive() throws Exception {
		List<Vertex> graph = FileReaderUtil.readGraphFromEdgeFile("/week4/test5.txt");
		
		List<Long> graphSCCs = StronglyConnectedComponent.computeGraphSCC(graph);
		
		assertEquals(Arrays.asList(6L, 3L, 2L, 1L), graphSCCs);
	}

}
