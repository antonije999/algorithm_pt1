package hr.antonije.test.main;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import hr.antonije.week1.test.MergeSortTest;
import hr.antonije.week2.test.QuickSortTest;
import hr.antonije.week3.test.RandomizedContractionTest;
import hr.antonije.week4.test.StronglyConnectedComponentTest;
import hr.antonije.week5.test.DijkstrasShortestPathTest;

@RunWith(Suite.class)
@SuiteClasses({MergeSortTest.class, QuickSortTest.class, RandomizedContractionTest.class,
			   StronglyConnectedComponentTest.class, DijkstrasShortestPathTest.class})
public class AllTests {

}
