package hr.antonije.week3.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import hr.antonije.graph.Vertex;
import hr.antonije.util.FileReaderUtil;
import hr.antonije.week3.core.RandomizedContraction;

public class RandomizedContractionTest {
	
	

	@Test
	public void randomizedContractionTestOne() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/test1.txt");
			
			assertEquals(2, RandomizedContraction.countGraphMinimumCut(vertexList, "/week3/test1.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void randomizedContractionTestTwo() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/test2.txt");
			
			assertEquals(2, RandomizedContraction.countGraphMinimumCut(vertexList, "/week3/test2.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void randomizedContractionTestThree() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/test3.txt");
			
			assertEquals(1, RandomizedContraction.countGraphMinimumCut(vertexList, "/week3/test3.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void randomizedContractionTestFour() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/test4.txt");
			
			assertEquals(1, RandomizedContraction.countGraphMinimumCut(vertexList, "/week3/test4.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void randomizedContractionTestFive() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/test5.txt");
			
			assertEquals(3, RandomizedContraction.countGraphMinimumCut(vertexList, "/week3/test5.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void randomizedContractionTestSix() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraph("/week3/test6.txt");
			
			assertEquals(2, RandomizedContraction.countGraphMinimumCut(vertexList, "/week3/test6.txt"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
