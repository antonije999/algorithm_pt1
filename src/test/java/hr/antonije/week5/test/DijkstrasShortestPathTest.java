package hr.antonije.week5.test;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import hr.antonije.graph.Vertex;
import hr.antonije.util.FileReaderUtil;
import hr.antonije.week5.core.DijkstrasShortestPath;

public class DijkstrasShortestPathTest {

	@Test
	public void testOne() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraphWithWeight("/week5/test1.txt");
			
			DijkstrasShortestPath.shortestPath(vertexList);
			
			assertEquals(0, vertexList.get(0).getMinPath());
			assertEquals(3, vertexList.get(1).getMinPath());
			assertEquals(3, vertexList.get(2).getMinPath());
			assertEquals(5, vertexList.get(3).getMinPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testTwo() {
		try {
			List<Vertex> vertexList = FileReaderUtil.readGraphWithWeight("/week5/test2.txt");
			
			DijkstrasShortestPath.shortestPath(vertexList);
			
			assertEquals(0, vertexList.get(0).getMinPath());
			assertEquals(3, vertexList.get(1).getMinPath());
			assertEquals(4, vertexList.get(2).getMinPath());
			assertEquals(5, vertexList.get(3).getMinPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
